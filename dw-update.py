#    Dreamwidth CLI updater
#    Copyright (C) 2019  antarcticite
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import mechanize
from bs4 import BeautifulSoup
import editor
import frontmatter
import getpass
import pydoc

br = mechanize.Browser()
br.open("https://dreamwidth.org/login")
br.select_form(action="https://www.dreamwidth.org/login")
br['user'] = input('Username: ')
br['password'] = getpass.getpass()
if br.submit().getcode() == 200:
    login_ok = True
    print("Login successful!")
else:
    login_ok = False
    print("Login failed")

br.open("https://dreamwidth.org/update")
br.select_form(action="update")


post = editor.edit(contents="---\nSubject: \nTags: \n---\n").decode('utf-8')
f = frontmatter.loads(post)

while True:
    preview = input("Show post preview? (y/n): ").strip()
    if preview == 'y': pydoc.pager(post)
    cont = input("Post? (p)\nEdit? (e)\nDiscard? (any key)\nYour choice: ").strip()
    if cont == 'p':
        br['subject'] = f['Subject']
        br['event'] = f.content
        br['prop_taglist'] = f['Tags']
        posting = br.submit()
        if posting.getcode() == 200:
            page_content = posting.read()
            post_url = BeautifulSoup(page_content, features="lxml").find("td").find("li").find("a")["href"]
            print("Posting successful! " + post_url)
        else:
            print("Posting failed")
        break
    elif cont == 'e':
        post = editor.edit(contents=post).decode('utf-8')
        f = frontmatter.loads(post)
        continue
    else: break
